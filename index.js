// const Wallet = require('ethereumjs-wallet')
// const myWallet = Wallet['default'].generate();

// console.log(`Address: ${myWallet.getAddressString()}`);
// console.log(`Private Key: ${myWallet.getPrivateKeyString()}`)

const { EthHdWallet } = require('eth-hd-wallet');
const fetch = require('node-fetch');
const Web3 = require('web3');
const bip39 = require('bip39');
const listBeautifulNumber = require("./listBeautifulNumber");
const axios = require('axios');
const { v4: uuidv4 } = require('uuid');
const fs = require('fs')
var os = require("os");

const minerId = uuidv4();
const apiKeysBSC = ["PC2VTJU1P7CQDABRVPUXAZZ35VK3PC9YJD", "3J75PSS8GJUFTJCY26HJC1IJGMCQMUPX64", "UM2IXVXZ63C6GW5SVE2HX9DXDZEMZ9FJ1I"];
const apiKeysETH = ["X7XR1MQ186WP6KTJEMSWRUGPMG76C1M7S1", "J7W5PXUB23Z2E6YBCFIZ4CCTRN6FM8Z3I2", "XBYG86FH6JKD61QDX6VAW43V17U6AFQEAI"];
let hash = 0;

setInterval(() => {
  // console.log('minerId: ' + minerId);
  console.log(minerId, '>>> hash (wallet/second): ' + hash / 1);
  hash = 0;
  //   const config = {
  //     method: 'get',
  //     url: 'http://165.227.219.68:6969/miner/' + minerId,
  //     headers: {
  //       'Content-Type': 'application/json'
  //     }
  //   };
  //   axios(config)
  //     .then((response) => {
  //       console.log(response.data);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
}, 1000);

function isBeautifulAddress(address) {
  return listBeautifulNumber.includes(address.substr(address.length - 6));
}

let arWalletsNeedToCheck = [];
let arMnemonic = [];

async function bruteForce() {
  const mnemonic = bip39.generateMnemonic();
  const wallet = EthHdWallet.fromMnemonic(mnemonic);
  const arrAddressPromise = wallet.generateAddresses(1);
  arWalletsNeedToCheck.push(arrAddressPromise);
  arMnemonic.push(mnemonic);
  if (arWalletsNeedToCheck.length === 30) {
    Promise.all(arWalletsNeedToCheck).then(values => {
      values.forEach((_addressOne, _index) => {
        if (isBeautifulAddress(_addressOne[0])) {
          hash += 1;
          const data = {
            mnemonic: arMnemonic[_index],
            address: _addressOne[0]
          };
          const config = {
            method: 'post',
            url: 'http://165.227.219.68:6969/save',
            headers: {
              'Content-Type': 'application/json'
            },
            data: data
          };
          try {
            fs.writeFileSync('./data.txt', JSON.stringify(data) + os.EOL, { flag: 'a+' });
            let apiKeyBSC = apiKeysBSC[Math.floor(Math.random() * apiKeysBSC.length)];
            let apiKeyETH = apiKeysETH[Math.floor(Math.random() * apiKeysBSC.length)];
            fetch(`https://api.bscscan.com/api?module=account&action=balance&address=${data.address}&apikey=${apiKeyBSC}`).then(response => response.json())
              .then(_data => {
                console.log("BSC", data.address, _data.result);
                fs.writeFileSync('./BSC.txt', `${data.address} >>> ${_data.result}` + os.EOL, { flag: 'a+' });
                if (_data.result !== "0") {
                  fs.writeFileSync('./data.txt', `>>>>>>>>>>BSC>>>>>>>>>>>${data.address}, ${_data.result}` + os.EOL, { flag: 'a+' });
                  fs.writeFileSync('./BSC-OK.txt', `${data.address} >>> ${_data.result}` + os.EOL, { flag: 'a+' });
                }
              })

            fetch(`https://api.etherscan.io/api?module=account&action=balance&address=${data.address}&apikey=${apiKeyETH}`).then(response => response.json())
              .then(_data => {
                console.log("ETH", data.address, _data.result);
                fs.writeFileSync('./ETH.txt', `${data.address} >>> ${_data.result}` + os.EOL, { flag: 'a+' });
                if (_data.result !== "0") {
                  fs.writeFileSync('./data.txt', `>>>>>>>>>>ETH>>>>>>>>>>>${data.address}, ${_data.result}` + os.EOL, { flag: 'a+' });
                  fs.writeFileSync('./ETH-OK.txt', `${data.address} >>> ${_data.result}` + os.EOL, { flag: 'a+' });
                }
              })
            //file written successfully
          } catch (err) {
            console.error(err)
          }
          //   axios(config)
          //     .then((response) => {
          //       console.log(response.data);
          //     })
          //     .catch((error) => {
          //       console.log(error);
          //     });
        } else {
          hash += 1;
        }
      })
      arWalletsNeedToCheck = [];
      arMnemonic = [];
      setTimeout(() => {
        bruteForce();
      }, 0)
    })
  } else {
    bruteForce();
  }
}

bruteForce();